﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    class Product
    {
        private string itemName = "";
        private string itemCategory = "";

        private int itemQuantity = 0;
        public int uniqueID = 0;
        private int itemSize = 0;

        private float itemPrice = 0;
        private float itemWeight = 0;

        public Product(string name, string category, int quantity, int ID, int size, float price, float weight)
        {
            itemName = name;
            itemCategory = category;

            itemQuantity = quantity;
            uniqueID = ID;
            itemSize = size;

            itemPrice = price;
            itemWeight = weight;
        }
    }
}
