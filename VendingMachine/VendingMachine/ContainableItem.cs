﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    class ContainableItem
    {
        private int row = 0;
        private int column = 0;
        public List<Product> productList = new  List<Product>();

        public ContainableItem()
        {
            var lines = System.IO.File.ReadAllLines(@"ItemFactory.txt");

            foreach (var line in lines)
            {
                
                var tabContent = line.Split(',');
                foreach(var data in tabContent)
                {
                    // Use a tab to indent each line of the file.

                    Console.WriteLine(data);
                }
            
            }
        }

        public int addProduct()     // add a product to the list 
        {
            return 0;
        }

        public int removeProduct()  //  remove a product from list
        {
            return 0;
        }

        public void display()  //display each item
        {
            foreach(var product in productList)
            {
                Console.WriteLine(product);
            }
            Console.ReadLine();
        }

    }
}
