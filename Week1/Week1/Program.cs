using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1
{
    class Program
    {
        static void Main(string[] args)
        {

            Dog dog = new Dog();
            dog.display();

            Duck duck = new Duck();
            duck.display();

            Snake snake = new Snake();
            snake.display();

            Elephant elephant = new Elephant();
            elephant.display();

            Console.ReadKey();
        }
    }
    
}
