﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1
{
    public class Animal
    {
        protected string animalName = "";
        protected string animalSound = "";

        public void display()
        {
            Console.WriteLine(animalName + " makes " + animalSound);
        }

    }

    public class Dog : Animal
    {

        public Dog()
        {
            animalName = "Dog";
            animalSound = "woof";
        }

    }

    public class Duck : Animal
    {

        public Duck()
        {
            animalName = "Duck";
            animalSound = "quack";
        }

    }

    public class Snake : Animal
    {

        public Snake()
        {
            animalName = "Snake";
            animalSound = "hisss";
        }

    }

    public class Elephant : Animal
    {

        public Elephant()
        {
            animalName = "Elephant";
            animalSound = "growl";
        }

    }

}
